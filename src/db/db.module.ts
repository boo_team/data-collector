import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_ADDRESS } from '../config';
import {
    LockSchema,
    LockSchemaKey,
    RawSearchResultSchema,
    RawSearchResultSchemaKey,
    SearchRequestSchema,
    SearchRequestSchemaKey,
} from '@bo/common';
import { Schema } from 'mongoose';

const mongooseModules = [
    MongooseModule.forRoot(MONGO_ADDRESS, { useNewUrlParser: true }),
    MongooseModule.forFeature([
        { name: SearchRequestSchemaKey, schema: SearchRequestSchema(Schema) },
        { name: RawSearchResultSchemaKey, schema: RawSearchResultSchema(Schema) },
        { name: LockSchemaKey, schema: LockSchema(Schema) },
    ]),
];

@Module({
    imports: [
        ...mongooseModules,
    ],
    exports: [
        ...mongooseModules,
    ],
})
export class DbModule {
}
