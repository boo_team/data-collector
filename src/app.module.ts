import { Module } from '@nestjs/common';
import { DbModule } from './db/db.module';
import { StatusModule } from './status/status.module';
import { ScrapModule } from './scrap/scrap.module';
import { SearchRequestModule } from './search-request/search-request.module';
import { CleanUpService } from './clean-up/clean-up.service';
import { CLEANING_UP_INTERVAL_MINUTES, EXPIRED_REQUEST_OCCUPANCY_MINUTES, STATUS_MONITOR_ADDRESS } from './config';
import { ScrapingRunnerService } from './scrap/scraping-runner.service';
import { SearchResultModule } from './search-result/search-result.module';
import { LockModule } from './lock/lock.module';
import { CleanUpModule } from './clean-up/clean-up.module';
import { AppController } from './app.controller';
import { SocketClient } from '@bo/common';

@Module({
    imports: [
        CleanUpModule,
        DbModule,
        LockModule,
        ScrapModule,
        StatusModule,
        SearchRequestModule,
        SearchResultModule,
    ],
    controllers: [AppController],
})
export class AppModule {
    constructor(
        private readonly socketClient: SocketClient,
        private readonly cleanUpService: CleanUpService,
        private readonly scrapingRunnerService: ScrapingRunnerService,
    ) {
    }

    public startService(): void {
        this.socketClient.connect(`${STATUS_MONITOR_ADDRESS}/sustain`);
        this.cleanUpService.start(CLEANING_UP_INTERVAL_MINUTES, EXPIRED_REQUEST_OCCUPANCY_MINUTES);
        this.scrapingRunnerService.start();
    }
}
