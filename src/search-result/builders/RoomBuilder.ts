import { BuilderType, BuilderWithoutNulls, ToBuilder } from '@bo/common';
import { Room } from '../interface/room';

export class RoomBuilder {
    public static get = (): BuilderType<Room> =>
        (new RoomToBuilder() as ToBuilder<Room>).toBuilder()
}

@BuilderWithoutNulls
class RoomToBuilder implements Room {
    public description: string = null;
    public personCount: string = null;
    public beds: string | null = null;
}
