export interface Room {
    description: string;
    personCount: string;
    beds: string | null;
}
