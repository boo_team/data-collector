import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { of, throwError } from 'rxjs';
import { model, Schema } from 'mongoose';
import { SearchRequestMaintainerService } from './search-request-maintainer.service';
import { LockSemaphoreService } from '../lock/lock-semaphore.service';
import { SearchRequestService } from './search-request.service';
import { LockRepository } from '../lock/lock.repository';
import {
    LockSchema,
    LockSchemaKey,
    SearchRequestDocument,
    SearchRequestSchema,
    SearchRequestSchemaKey, SearchRequestStatusDto,
    SocketClient,
    StatusEvent,
} from '@bo/common';

const lockModel = model(LockSchemaKey, LockSchema(Schema));
const searchRequestModel = model(SearchRequestSchemaKey, SearchRequestSchema(Schema));

class SocketClientMock {
    connect = () => {
        return;
    }
    watchEvent = () => of();
    sendIfConnected = () => {
        return;
    }
}

describe('SearchRequestService', () => {

    let testModule: TestingModule;

    beforeEach(async () => {
        testModule = await Test.createTestingModule({
            providers: [
                SearchRequestService,
                SearchRequestMaintainerService,
                { provide: getModelToken(SearchRequestSchemaKey), useValue: searchRequestModel },
                LockSemaphoreService,
                LockRepository,
                { provide: getModelToken(LockSchemaKey), useValue: lockModel },
                { provide: SocketClient, useClass: SocketClientMock },
            ],
        }).compile();
    });

    describe('acquireSearchRequest', () => {

        let searchRequestService: SearchRequestService;
        let searchRequestMaintainerService: SearchRequestMaintainerService;
        let lockRepository: LockRepository;
        let socketClient: SocketClient;

        beforeEach(() => {
            searchRequestService = testModule.get<SearchRequestService>(SearchRequestService);
            searchRequestMaintainerService = testModule.get<SearchRequestMaintainerService>(SearchRequestMaintainerService);
            lockRepository = testModule.get<LockRepository>(LockRepository);
            socketClient = testModule.get<SocketClient>(SocketClient);
        });

        it('should propagate acquired free search request', () => {
            // GIVEN
            jest.spyOn(searchRequestMaintainerService, 'getFreeAndValidatedSearchRequest')
                .mockReturnValue(of({ searchId: 'mock' } as SearchRequestDocument));
            jest.spyOn(searchRequestMaintainerService, 'occupyAndGetUpdatedRequest')
                .mockReturnValue(of({ searchId: 'mock' } as SearchRequestDocument));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(socketClient, 'sendIfConnected');

            // WHEN
            searchRequestService.acquireSearchRequest();

            // THEN
            searchRequestService.onSearchRequest().subscribe((v) => {
                expect(v.searchId).toBe('mock');
            });
            expect(socketClient.sendIfConnected)
                .toHaveBeenCalledWith(StatusEvent.SEARCH_REQUEST_UPDATE, { searchId: 'mock', timestamp: expect.any(Number) });
        });

        it('should propagate acquired new search request', () => {
            // GIVEN
            jest.spyOn(socketClient, 'watchEvent').mockReturnValue(of({ searchId: 'mock' } as SearchRequestStatusDto));
            jest.spyOn(searchRequestMaintainerService, 'getFreeAndValidatedSearchRequest').mockReturnValue(of(null));
            jest.spyOn(searchRequestMaintainerService, 'findNewSearchRequest')
                .mockReturnValue(of({ searchId: 'mock' } as SearchRequestDocument));
            jest.spyOn(searchRequestMaintainerService, 'occupyAndGetUpdatedRequest')
                .mockReturnValue(of({ searchId: 'mock' } as SearchRequestDocument));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(socketClient, 'sendIfConnected');

            // WHEN
            searchRequestService.acquireSearchRequest();

            // THEN
            searchRequestService.onSearchRequest().subscribe((v) => {
                expect(v.searchId).toBe('mock');
            });
            expect(socketClient.sendIfConnected)
                .toHaveBeenCalledWith(StatusEvent.SEARCH_REQUEST_UPDATE, { searchId: 'mock', timestamp: expect.any(Number) });
        });

        it('should not propagate search request if request not found', () => {
            // GIVEN
            jest.spyOn(searchRequestMaintainerService, 'getFreeAndValidatedSearchRequest').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(socketClient, 'sendIfConnected');

            // WHEN
            searchRequestService.acquireSearchRequest();

            // THEN
            searchRequestService.onSearchRequest().subscribe();
            expect(socketClient.sendIfConnected).not.toHaveBeenCalled();
        });

        it('should not propagate search request if requesting is locked', () => {
            // GIVEN
            jest.spyOn(searchRequestMaintainerService, 'getFreeAndValidatedSearchRequest')
                .mockReturnValue(of({ searchId: 'mock' } as SearchRequestDocument));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(throwError(new Error('E11000')));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'findOneAndDelete').mockReturnValue(of(null));
            jest.spyOn(socketClient, 'sendIfConnected');

            // WHEN
            searchRequestService.acquireSearchRequest();

            // THEN
            searchRequestService.onSearchRequest().subscribe();
            expect(socketClient.sendIfConnected).not.toHaveBeenCalled();
        });

        it('should not propagate search request if error when requesting', () => {
            // GIVEN
            jest.spyOn(searchRequestMaintainerService, 'getFreeAndValidatedSearchRequest').mockReturnValue(throwError(new Error('mock')));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(socketClient, 'sendIfConnected');

            // WHEN
            searchRequestService.acquireSearchRequest();

            // THEN
            searchRequestService.onSearchRequest().subscribe();
            expect(socketClient.sendIfConnected).not.toHaveBeenCalled();
        });
    });
});
