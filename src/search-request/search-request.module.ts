import { Module } from '@nestjs/common';
import { SearchRequestService } from './search-request.service';
import { StatusModule } from '../status/status.module';
import { SearchRequestMaintainerService } from './search-request-maintainer.service';
import { LockModule } from '../lock/lock.module';
import { DbModule } from '../db/db.module';

@Module({
    imports: [
        DbModule,
        LockModule,
        StatusModule,
    ],
    providers: [
        SearchRequestService,
        SearchRequestMaintainerService,
    ],
    exports: [
        SearchRequestService,
    ],
})
export class SearchRequestModule {
}
