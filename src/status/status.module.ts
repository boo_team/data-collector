import { Module } from '@nestjs/common';
import { SocketClient } from '@bo/common';
import { logger } from '../config';

@Module({
    providers: [
        {
            provide: SocketClient,
            useFactory: () => new SocketClient(logger),
        },
    ],
    exports: [
        SocketClient,
    ],
})
export class StatusModule {
}
