import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';
import { logger } from './config';

@Controller('api/v1/status')
export class AppController {

    @Get()
    getStatus(@Res() res: Response): void {
        logger.info('Requesting for status...');
        res.sendStatus(HttpStatus.OK);
    }
}
