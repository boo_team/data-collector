import { NestFactory } from '@nestjs/core';
import { LoggerService } from '@nestjs/common';
import { AppModule } from './app.module';
import { logger, PORT } from './config';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        logger: logger as LoggerService,
    });
    await app.listen(PORT);
    app.get(AppModule).startService();
}

bootstrap();
