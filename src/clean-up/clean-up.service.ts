import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { logger } from '../config';
import { OccupancyStatus } from '../search-request/enum/OccupancyStatus';
import { AsyncHelper, SearchRequestDocument, SearchRequestSchemaKey, SearchRequestStatusDto, SocketClient, StatusEvent } from '@bo/common';
import { LockSemaphoreService } from '../lock/lock-semaphore.service';
import { from, interval } from 'rxjs';
import { filter, startWith, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class CleanUpService {

    constructor(
        @InjectModel(SearchRequestSchemaKey) private readonly searchRequestModel: Model<SearchRequestDocument>,
        private readonly lockSemaphoreService: LockSemaphoreService,
        private readonly socketClient: SocketClient,
    ) {
    }

    public start(cleaningUpIntervalMin: number, expiredRequestOccupancyMin: number): void {
        const cleaningUpIntervalMs = cleaningUpIntervalMin * 60000;
        const expiredRequestOccupancyMs = expiredRequestOccupancyMin * 60000;
        logger.info(`Invoking automatic cleaning up mechanism. ` +
            `Cleaning will be performed every [${cleaningUpIntervalMin}] minutes. ` +
            `Expired requests occupancy will be free after [${expiredRequestOccupancyMin}] minutes of idle status.`);
        interval(cleaningUpIntervalMs).pipe(
            startWith(0),
            tap((counter) => logger.debug(`Clean up process start. Clean up has been performed [${counter}] times.`)),
            switchMap(() => this.lockSemaphoreService.lockProcessingAction(
                from(this.cleanUpExpiredRequestsAndNotify(expiredRequestOccupancyMs))),
            ),
        ).subscribe(() => {
            logger.debug('Clean up process finish.');
        });
        this.lockSemaphoreService.unlockIfExpired()
            .pipe(filter((v) => Boolean(v)))
            .subscribe((expired) => {
                logger.warn(`Unlocking expired lock on service start. Lock was created at: ${expired.createdAt}`);
            });
    }

    /**
     * Set 'occupancyStatus' value of search requests with expired occupancy to 'FREE'.
     */
    public async cleanUpExpiredRequestsAndNotify(expiredThresholdMs: number): Promise<void> {
        const lessThanOccupancyMin = Date.now() - expiredThresholdMs;
        const foundSearchRequests = await this.searchRequestModel.find(
            {
                occupancyStatus: { $ne: OccupancyStatus.FREE },
                occupancyUpdatedAt: { $lt: lessThanOccupancyMin },
            }).exec();

        if (foundSearchRequests && foundSearchRequests.length) {
            await AsyncHelper.forEach(foundSearchRequests, async (searchRequestDocument: SearchRequestDocument) => {
                const updatedReq = await this.searchRequestModel.findOneAndUpdate(
                    { searchId: searchRequestDocument.searchId } as SearchRequestDocument,
                    { occupancyStatus: OccupancyStatus.FREE } as SearchRequestDocument,
                    { new: true });
                this.socketClient.sendIfConnected<SearchRequestStatusDto>(StatusEvent.SEARCH_REQUEST_UPDATE, {
                    timestamp: Date.now(),
                    searchId: searchRequestDocument.searchId,
                });
                logger.info(`Occupancy status: '${searchRequestDocument.occupancyStatus}' for search queue with searchId: ` +
                    `'${updatedReq.searchId}', was set to '${updatedReq.occupancyStatus}' because of occupancy expiration.`);
            });
        }
    }
}
