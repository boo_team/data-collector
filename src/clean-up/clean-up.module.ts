import { Module } from '@nestjs/common';
import { LockModule } from '../lock/lock.module';
import { DbModule } from '../db/db.module';
import { StatusModule } from '../status/status.module';
import { CleanUpService } from './clean-up.service';

@Module({
    imports: [
        DbModule,
        LockModule,
        StatusModule,
    ],
    providers: [
        CleanUpService,
    ],
    exports: [
        CleanUpService,
    ],
})
export class CleanUpModule {
}
