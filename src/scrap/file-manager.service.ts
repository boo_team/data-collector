import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { logger } from '../config';
import { Page } from 'puppeteer';
import { SearchResult } from '../search-result/interface/searchResult';

@Injectable()
export class FileManagerService {

    private readonly RESULTS_FOLDER_PATH = 'results/';

    public async saveHotelDataAsJSON(searchResult: SearchResult, formattedName: string): Promise<string> {
        const jsonPath = `${this.RESULTS_FOLDER_PATH}${this.getActualFormattedDate()}-${formattedName}.json`;
        await new Promise<void>((resolve) => {
            fs.writeFile(jsonPath, JSON.stringify(searchResult), (err: NodeJS.ErrnoException): void => {
                if (err) {
                    logger.error(`Error when writing file '${jsonPath}'.`, err);
                }
                resolve();
            });
        });
        return jsonPath;
    }

    public async takeScreenshot(page: Page, name: string): Promise<void> {
        await page.screenshot({ path: `${this.RESULTS_FOLDER_PATH}${this.getActualFormattedDate()}-${name}.png` });
    }

    private getActualFormattedDate = (): string => new Date().toISOString().replace(/[^0-9]/g, '');
}
