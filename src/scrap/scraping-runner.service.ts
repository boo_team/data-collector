import { Injectable } from '@nestjs/common';
import { BrowserService } from './browser.service';
import { ScrapHotelsScenarioService } from './scrap-hotels-scenario.service';
import { FileManagerService } from './file-manager.service';
import { HotelDataAggregatorService } from '../search-result/hotel-data-aggregator.service';
import { SearchResultService } from '../search-result/search-result.service';
import { SearchResult } from '../search-result/interface/searchResult';
import { SearchRequestService } from '../search-request/search-request.service';
import { ScrapingStatusDto, SearchRequestDocument, SocketClient, StatusEvent } from '@bo/common';
import { logger, SAVE_RESULTS_AS_JSON_AND_TAKE_SCREENSHOTS_ON_ERROR } from '../config';

@Injectable()
export class ScrapingRunnerService {

    constructor(
        private readonly browserService: BrowserService,
        private readonly fileManagerService: FileManagerService,
        private readonly hotelDataAggregatorService: HotelDataAggregatorService,
        private readonly scrapHotelsScenarioService: ScrapHotelsScenarioService,
        private readonly searchRequestService: SearchRequestService,
        private readonly searchResultService: SearchResultService,
        private readonly socketClient: SocketClient,
    ) {
    }

    public start() {
        this.searchRequestService.acquireSearchRequest();
        this.searchRequestService.onSearchRequest()
            .subscribe(async (searchRequestDocument: SearchRequestDocument) => {
                try {
                    logger.debug('Initialize browser and navigate to target page.');
                    await this.browserService.initBrowserAndOpenBlankPage();
                    logger.info('Starting search process and then scraping.');
                    const searchResult = await this.startScenario(searchRequestDocument);
                    if (searchResult && SAVE_RESULTS_AS_JSON_AND_TAKE_SCREENSHOTS_ON_ERROR) {
                        const pathToResult = await this.fileManagerService.saveHotelDataAsJSON(searchResult,
                            `${searchRequestDocument.city}_${searchRequestDocument.searchId}`);
                        logger.info(`HotelData was saved locally to: ${pathToResult}`);
                    }
                } catch (e) {
                    logger.error('Error during running the scenario process.', e);
                } finally {
                    const pagesBefore = await this.browserService.pagesCount() || [];
                    logger.debug('Closing browser. Open pages: ', pagesBefore.map((p) => ({ url: p.url() })));
                    await this.browserService.closeBrowser();
                    const pagesAfter = await this.browserService.pagesCount() || [];
                    logger.debug('Browser closed. Open pages: ', pagesAfter.map((p) => ({ url: p.url() })));
                }
                this.searchRequestService.acquireSearchRequest();
            });
    }

    private async startScenario(searchRequest: SearchRequestDocument): Promise<SearchResult> {
        const startSearchProcessTimeMs = Date.now();
        const searchId = searchRequest.searchId;
        try {
            const { hotels, scrapingStatusDto, searchPerformedForPlace } =
                await this.scrapHotelsScenarioService.collectHotels(searchRequest);
            logger.debug(`Preparing search results. Number of collected hotels: ${hotels.length}.`);
            const searchResult = this.hotelDataAggregatorService.prepareSearchResult(
                searchId, searchPerformedForPlace, startSearchProcessTimeMs, scrapingStatusDto.scrapingTimeSeconds, hotels);
            logger.debug(`Search results with id: ${searchResult.searchId} prepared. ` +
                `Number of collected hotels: ${searchResult.hotels.length}. ` +
                `Time of whole process - collecting and preparing results: ${searchResult.searchProcessTimeSeconds} sec.`);
            logger.debug(`Creating new search result for search id: ${searchId}.`);
            await this.searchResultService.create(searchResult);
            logger.info(`New search result for search id: ${searchId} was created.`);
            this.socketClient.sendIfConnected<ScrapingStatusDto>(StatusEvent.SCRAPING_STATUS, {
                ...scrapingStatusDto,
                savedToDb: true,
                timestamp: Date.now(),
            });
            return searchResult;
        } catch (e) {
            logger.error(`Error when collecting data from web page. Search id: [${searchRequest.searchId}].`, e);
            if (SAVE_RESULTS_AS_JSON_AND_TAKE_SCREENSHOTS_ON_ERROR) {
                await this.browserService.takeScreenshot('error');
            }
            return e;
        } finally {
            this.searchRequestService.freeSearchRequestAndNotify(searchId);
        }
    }
}
