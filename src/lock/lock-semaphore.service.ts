import { Injectable } from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { EXPIRED_LOCK_SECONDS, logger } from '../config';
import { LockRepository } from './lock.repository';
import { LockDocument } from '@bo/common';

@Injectable()
export class LockSemaphoreService {

    private readonly LOCK_ERROR = 'E11000';

    constructor(private readonly lockRepository: LockRepository) {
    }

    public lockProcessingAction<T>(action: Observable<T>): Observable<T> {
        const lockStart = Date.now();
        return this.lockRepository.lock().pipe(
            tap(() => logger.debug('Locking cleaning up and requesting process.')),
            switchMap(() => action),
            switchMap((t: T) =>
                this.unlockAndLog(lockStart).pipe(
                    switchMap(() => {
                        if (t) {
                            return of(t);
                        } else {
                            return of();
                        }
                    }),
                ) as Observable<T>),
            catchError((err: Error) => {
                if ((err).message.includes(this.LOCK_ERROR)) {
                    return this.onLockError()
                        .pipe(switchMap(() => of())) as Observable<T>;
                } else {
                    return this.onErrorDuringProcessingAction(err, lockStart)
                        .pipe(switchMap(() => of())) as Observable<T>;
                }
            }),
        );
    }

    public unlockIfExpired(): Observable<LockDocument | null> {
        const lessThanLockedTime = Date.now() - (EXPIRED_LOCK_SECONDS * 1000);
        return this.lockRepository.findOneAndDelete({
            createdAt: { $lt: lessThanLockedTime },
        });
    }

    private onLockError(): Observable<LockDocument> {
        logger.warn(`The action has been suspended. Clean up or acquiring a search request operations still processing.`);
        return this.unlockIfExpired().pipe(
            tap((expired: LockDocument) => {
                if (expired) {
                    logger.warn(`Unlocking expired lock. Lock was created at: ${expired.createdAt}`);
                }
            }),
        );
    }

    private onErrorDuringProcessingAction(err: Error, lockStart: number): Observable<LockDocument> {
        logger.error('Error when cleaning up or acquiring a search request: ', err);
        return this.unlockAndLog(lockStart);
    }

    private unlockAndLog(lockStart: number): Observable<LockDocument> {
        return this.lockRepository.unlock().pipe(
            tap(() => logger.info(`Unlocked cleaning up and requesting process. Locking lasted [${Date.now() - lockStart}] ms.`)),
        );
    }
}
