import { Module } from '@nestjs/common';
import { DbModule } from '../db/db.module';
import { LockSemaphoreService } from './lock-semaphore.service';
import { LockRepository } from './lock.repository';

@Module({
    imports: [
        DbModule,
    ],
    providers: [
        LockSemaphoreService,
        LockRepository,
    ],
    exports: [
        LockSemaphoreService,
    ],
})
export class LockModule {
}
