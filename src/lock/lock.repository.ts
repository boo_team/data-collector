import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { ObjectId } from 'bson';
import { LockDocument, LockSchemaKey } from '@bo/common';

@Injectable()
export class LockRepository {

    private readonly LOCK_ID: ObjectId = ObjectId.createFromHexString('64617461636f6c6c6563746f'); // Hex from 'datacollecto'

    constructor(
        @InjectModel(LockSchemaKey) private readonly lockModel: Model<LockDocument>,
    ) {
    }

    public findOneAndDelete(conditions: any): Observable<LockDocument | null> {
        return from(this.lockModel.findOneAndDelete(conditions).exec());
    }

    public lock(): Observable<LockDocument> {
        return from(new this.lockModel({ _id: this.LOCK_ID }).save());
    }

    public unlock(): Observable<LockDocument> {
        return from(this.lockModel.findByIdAndDelete(this.LOCK_ID).exec());
    }
}
