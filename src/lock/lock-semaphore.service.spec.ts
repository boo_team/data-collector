import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { LockSemaphoreService } from './lock-semaphore.service';
import { of, throwError } from 'rxjs';
import { model, Schema } from 'mongoose';
import { LockRepository } from './lock.repository';
import { LockSchema, LockSchemaKey } from '@bo/common';

const lockModel = model(LockSchemaKey, LockSchema(Schema));

describe('LockSemaphoreService', () => {

    let testModule: TestingModule;

    beforeEach(async () => {
        testModule = await Test.createTestingModule({
            providers: [LockSemaphoreService, LockRepository, { provide: getModelToken(LockSchemaKey), useValue: lockModel }],
        }).compile();
    });

    describe('lockProcessingAction', () => {

        let lockSemaphoreService: LockSemaphoreService;
        let lockRepository: LockRepository;

        beforeEach(() => {
            lockSemaphoreService = testModule.get<LockSemaphoreService>(LockSemaphoreService);
            lockRepository = testModule.get<LockRepository>(LockRepository);
        });

        it('should unlock after performing an action', () => {
            // GIVEN
            const action = of(null);
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));

            // WHEN
            lockSemaphoreService.lockProcessingAction(action).subscribe();

            // THEN
            expect(lockRepository.unlock).toBeCalled();
        });

        it('should unlock when error', () => {
            // GIVEN
            const action = throwError(new Error('mock'));
            jest.spyOn(lockRepository, 'lock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));

            // WHEN
            lockSemaphoreService.lockProcessingAction(action).subscribe();

            // THEN
            expect(lockRepository.unlock).toBeCalled();
        });

        it('should not unlock when lock error', () => {
            // GIVEN
            const action = of(null);
            jest.spyOn(lockRepository, 'lock').mockReturnValue(throwError(new Error('E11000')));
            jest.spyOn(lockRepository, 'unlock').mockReturnValue(of(null));
            jest.spyOn(lockRepository, 'findOneAndDelete').mockReturnValue(of(null));

            // WHEN
            lockSemaphoreService.lockProcessingAction(action).subscribe();

            // THEN
            expect(lockRepository.unlock).not.toBeCalled();
        });
    });
});
